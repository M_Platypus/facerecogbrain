import React from 'react'

function Rank() {
    return (
        <div>
            <div className='white f3'>
                {'Amelia, your current rank is...'}
            </div>
            <div className='white f1'>
                {'#2'}
            </div>
        </div>
    )
}

export default Rank
